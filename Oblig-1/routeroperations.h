#ifndef ROUTEROPERATIONS_H_
#define ROUTEROPERATIONS_H_

// Hentet disse fra plenums-timene

#define MASK_BIT_N(n)       (1 << n)
#define MASK_NOT_BIT_N(n)  ~(1 << n)
#define GET_BIT_N(a, n)     ((0u == (a & (1<<n)))?0u:1u)
#define SET_BIT_N(a, n)     (a |= MASK_BIT_N(n))
#define CLEAR_BIT_N(a, n)   (a &= MASK_NOT_BIT_N(n))

#endif
