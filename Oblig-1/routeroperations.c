#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "routeroperations.h"

// Variables are ordered by their size, to allow for less padding in memory
typedef struct Routers {
    unsigned char id;                        // 1
    unsigned char FLAG;                      // 1
    unsigned char nameLength;                // 1
    int numCons;                             // 4
    char* name;                              // 8
    struct Routers *connections[10];         // 8*10 
} Router;                                    // 96  (1 in padding)

int numRouters;
Router **routers;
// Used to keep track of how big the array actually is
int len_routers;

// Returns pointer to router with given id, if it exists
// Arguments: int router_id
Router *get_pointer(int id) {
    int i;
    for (i = 0; i < len_routers; i++) {
        if (routers[i] == NULL) {
            continue;
        }
        if (routers[i]->id == id)
            return routers[i];
    }
    return NULL;
}

// Creates routers and stores them in **routers
// Arguments: string(char*) filename_to_read_from
Router **create_routers_from_file(char *filename) {
    FILE *file = fopen(filename, "r");
    if(file == NULL) {
        printf("Router file not found\n");
        exit(EXIT_FAILURE);
    }
    fread(&numRouters, sizeof(int), 1, file);
    
    // Allocates memory for Router pointers, each at 8 bytes
    // totaling (8 * numRouters) bytes
    routers = malloc(sizeof(Router*)*numRouters);
    len_routers = numRouters;
    
    // Reads router-info
    int i;
    for (i = 0; i < numRouters; i++) {
        // Allocates memory for a Router, at 96 bytes
        routers[i] = malloc(sizeof(Router));
        fseek(file, 1, SEEK_CUR); // Jump past \n 
        
        // Reading to struct from file
        fread(&routers[i]->id,         sizeof(unsigned char), 1, file);
        fread(&routers[i]->FLAG,       sizeof(unsigned char), 1, file);
        fread(&routers[i]->nameLength, sizeof(unsigned char), 1, file);
        
        // Allocating memory for router name, 1 byte per character + nullbyte
        routers[i]->name = malloc(routers[i]->nameLength);
        fgets(routers[i]->name, routers[i]->nameLength, file);
        routers[i]->numCons = 0;
    }

    // Reads connections
    unsigned char buffer[2];  
    fseek(file, 1, SEEK_CUR);
    while (fread(buffer, sizeof(unsigned char), 2, file) == 2) {
        Router *ptr_1 = get_pointer(buffer[0]);
        ptr_1->connections[ptr_1->numCons++] = get_pointer(buffer[1]);
        fseek(file, 1, SEEK_CUR); // Jumps past \n
    }

    fclose(file);
    printf("Successfully read %d routers from file\n", numRouters);
    return routers;
}

// Returns a given bit from flag in router with given id
// Arguments: int router_id, int bit_position
unsigned char get_flag_bit(int id, int bit) {
    return GET_BIT_N(get_pointer(id)->FLAG, bit);
}

// Returns the change-number from router with given id
int get_change_num(int id) {
    return (get_pointer(id)->FLAG & 0xF0) >> 4;
}

// Prints router with id id
// Arguments: int router_id
void printRouter(int id) {
    Router *router = get_pointer(id);
    if (router == NULL) return;

    printf("\nid: \t\t%d\n", router->id);
    printf("name: \t\t%s\n", router->name);
    printf("nameLen: \t%d\n", router->nameLength);

    printf("aktiv: \t\t%s\n", get_flag_bit(id, 0) ? "ja" : "nei"); 
    printf("trådløs: \t%s\n", get_flag_bit(id, 1) ? "ja" : "nei"); 
    printf("5GHz: \t\t%s\n", get_flag_bit(id, 2) ? "ja" : "nei");

    printf("endringsnummer: %i\n", get_change_num(id));
    printf("numCons: \t%d\n", router->numCons);
    
    int j;
    printf("edges to: \t");
    for (j = 0; j < router->numCons; j++) {
       printf("%d, ", router->connections[j]->id);
    }
    printf("\n");
}

void printRouters() {
    int i;
    // Goes through all possible IDs
    for(i = 0; i < len_routers; i++)
        printRouter(i);
    printf("\n");
}

// Sets flag value
// Argument string format: "%id %flag %value"
void set_flag(char *args) {
    int id    = atoi(strtok(args, " "));
    int flag  = atoi(strtok(NULL, " "));
    int value = atoi(strtok(NULL, " "));

    if (flag == 3 || flag > 4 || flag < 0) {
        printf("Invalid flag, aborting\n");
        return;
    }

    printf("Setting router %ds flag %d, to %d\n", id, flag, value);

    Router *router = get_pointer(id);
    if (router == NULL) {
        printf("Router with id: %d, does not exist\n", id);
        return;
    }

    if (flag != 4) {
        if (value == 0)
            router->FLAG = CLEAR_BIT_N(router->FLAG, flag);
        else if (value == 1)
            router->FLAG = SET_BIT_N(router->FLAG, flag);
        else
            printf("Invalid value %d, should be 1 or 0\n", value);
    } else {
        if (value < 0 || value > 15) {
            printf("FLAG Value not in legal range [0-15]!\n");
            return;
        }
        router->FLAG = (router->FLAG & 0x0F) | (value << 4);
    }
}

// Sets router-name of router with given id
// Argument string format: "%id %name"
void set_model(char *args) {
    int id = atoi(strtok(args, " "));
    char * newName = strtok(NULL, "\n");
    
    int newLength = strlen(newName) + 1;
    Router *ptr_r = get_pointer(id);
    printf("Changing '%s' to '%s'\n", ptr_r->name, newName); 
    // Freeing memory used for previous name, size: (1 * oldLength) bytes
    // was allocated at line 84
    free(ptr_r->name);
    // Allocating memory for the new name, size: (1 * newLength) bytes
    ptr_r->name = malloc(sizeof(char) * newLength);
    strcpy(ptr_r->name, newName);
    ptr_r->nameLength = newLength;
}

// Adds connection from router a to router b
// Argument string: "%a_id %b_id"
void add_connection(char *args) {
    Router *ptr_id   = get_pointer(atoi(strtok(args, " ")));
    Router *ptr_id_2 = get_pointer(atoi(strtok(NULL, " ")));

    if (ptr_id == NULL || ptr_id_2 == NULL) {
        printf("Invalid router-id\n");
        return;
    }
     ptr_id->connections[ptr_id->numCons++] = ptr_id_2;
     printf("Added connection from %d to %d\n", ptr_id->id, ptr_id_2->id);
}

// Removes a connection from *router, at index index
// Arguments: int index, Router* router_to_remove_from
void remove_connection(int index, Router *router) {
    int i;
    for (i = index; i < router->numCons - 1; i++) {
        router->connections[i] = router->connections[i+1];
    }
    router->numCons--;
}

// Deletes a router, and connections to the router
// Arguments: int router_id
void delete_router(int id) {
    // deleting connections to this router
    int i;
    for (i = 0; i < len_routers; i++) {
        int j;
        Router *ptr = routers[i];
        if (ptr == NULL) continue;
        for (j = 0; j < ptr->numCons; j++) {
            if (ptr->connections[j]->id == id) {
                remove_connection(j, routers[i]);
            }
        }
    }
    // Finding router index in routers[]
    int router_index = -1;
    for (i = 0; i < len_routers; i++) {
        if (routers[i] == NULL)
            continue;
        if (routers[i]->id == id)
            router_index = i;
    }
    if (router_index == -1) {
        printf("No router with that id\n");
        return;
    }

    // Freeing memory used by router
    free(routers[router_index]->name);  // Allocated at line 84
    free(routers[router_index]);        // Allocated at line 75 
    routers[router_index] = NULL;

    printf("Deleted router %d\n", id);
    numRouters--;
}


char * visited;

// Standard Depth-first search
// Arguments: Router* from, Router* to
int DFS(Router *from, Router *to) {
    visited[from->id] = 1;
    if (from->id == to->id)
        return 1;
    
    int i;
    for (i = 0; i < from->numCons; i++) 
        if (visited[from->connections[i]->id] == 0)
             if (DFS(from->connections[i], to) == 1)
                return 1; 
    return 0;
}

// Starts a DFS to see if a path exists between point a and b
// Argument string: "%a_id %b_id"
void exists_path(char *args) {
    // Antar mengden IDer er i [0..numRouters-1]
    // Allocates (1 * length of Router*_array) bytes. 
    visited = malloc(sizeof(char) * len_routers);
    memset(visited, 0x00, sizeof(char) * len_routers);

    Router *ptr_from = get_pointer(atoi(strtok(args, " ")));
    Router *ptr_to   = get_pointer(atoi(strtok(NULL, " ")));

    if (ptr_from == NULL || ptr_to == NULL) {
        printf("Invalid router-id\n");
        return;
    }

    if (DFS(ptr_from, ptr_to) == 1) 
        printf("Found path from %d to %d\n", ptr_from->id, ptr_to->id);
    else
        printf("Did not find path from %d to %d\n", ptr_from->id, ptr_to->id);
    
    // Allocated at line 279
    free(visited);
}

// Runs functions depending on *cmd, passes args along
// Arguments: string(char*) command, string(char*) arguments
void exec_command(char *cmd, char *args) {
     
    printf("\n--> %s%s\n", cmd, args);
    if      (strcmp(cmd, "print") == 0)              printRouter(atoi(args));
    else if (strcmp(cmd, "sett_flag") == 0)          set_flag(args);  //printf("sett flag\n");
    else if (strcmp(cmd, "sett_modell") == 0)        set_model(args);
    else if (strcmp(cmd, "legg_til_kobling") == 0)   add_connection(args);
    else if (strcmp(cmd, "slett_router") == 0)       delete_router(atoi(args));
    else if (strcmp(cmd, "finnes_rute") == 0)        exists_path(args);
    else                                             printf("Invalid command\n");
}

// Reads from file, and runs commands line by line from it.
// Arguments: string(char*) filename
void parse_commands(char * filename) {
    printf("\nExecuting commands\n");
    
    FILE *file = fopen(filename, "r");
    if(file == NULL) {
        printf("Commands file not found\n");
        exit(EXIT_FAILURE);
    }
    
    char command[20];
    int scanned;
    while ((scanned = fscanf(file, "%s", command)) != EOF) {
        char args[30];
        fscanf(file, "%30[^\n]", args); 
        exec_command(command, args);
    }
    fclose(file);
}

// Saves all routers and their connections to file
// Arguments: string(char*) filename
void write_to_file(char * filename) {
    FILE *file = fopen(filename, "w+");
    
    fwrite(&numRouters, sizeof(int), 1, file);
    fwrite("\n", sizeof(char), 1, file);

    int i;  // Writes router-info to file
    for (i = 0; i < len_routers; i++) {
        if (routers[i] == NULL) continue;
        fwrite(&routers[i]->id,         sizeof(unsigned char), 1, file);
        fwrite(&routers[i]->FLAG,       sizeof(unsigned char), 1, file);
        fwrite(&routers[i]->nameLength, sizeof(unsigned char), 1, file);
        fwrite(routers[i]->name,        sizeof(char), routers[i]->nameLength - 1, file);
        fwrite("\n", sizeof(char), 1, file);
    }
     // Writes connections to file
    for (i = 0; i < len_routers; i++) {
        int j;
        if (routers[i] == NULL) continue;
        for (j = 0; j < routers[i]->numCons; j++) {
            fwrite(&routers[i]->id, sizeof(char), 1, file);
            fwrite(&routers[i]->connections[j]->id, sizeof(char), 1, file);
            fwrite("\n", sizeof(char), 1, file);
        }
    }
    fclose(file);
}

// Frees memory used by routers
void freeRouters() {
    int i;
    for (i = 0; i < len_routers; i++) {
        if(routers[i] == NULL) continue;
        free(routers[i]->name); // Allocated at line 84
        free(routers[i]);       // Allocated at line 75
    }
    free(routers);      // Allocated at line 67 
}


int main(int argc, char** argv) {
    if (argc != 3) {
        printf("FAILURE: Program takes 2 arguments!\n");
        exit(EXIT_FAILURE);
    }
    
    char *router_info = argv[1];
    char *router_cmds = argv[2];
    
    create_routers_from_file(router_info);
    parse_commands(router_cmds);
    write_to_file(router_info);

    freeRouters();
    printf("\nFinished\n");
    return 0;
}
