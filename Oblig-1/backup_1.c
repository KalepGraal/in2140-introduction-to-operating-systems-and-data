#include <stdio.h>
#include <stdlib.h>

typedef struct Routers {
    unsigned char id;
    unsigned char FLAGG;
    unsigned char nameLength;
    char* name;
} Router;

Router *create_nodes_from_file(char *filename) {
    int numRouters;

    FILE *file = fopen(filename, "r");
    fread(&numRouters, sizeof(int), 1, file);

    printf("Number of routers: %d\n", numRouters);

    Router *routers = malloc(sizeof(Router)*numRouters);

    int i;
    for (i = 0; i < numRouters; i++) {
        fseek(file, 1, SEEK_CUR); // Jump past \n ?
        int router_id;
        fread(&router_id, sizeof(unsigned char), 1, file);
        //printf("router_id: %d\n", router_id);

        unsigned char FLAGG;
        fread(&FLAGG, sizeof(unsigned char), 1, file);

        int nameLength;
        fread(&nameLength, sizeof(unsigned char), 1, file); 
        //printf("name_length: %d\n", nameLength);
       
        char *name = (char *) malloc(nameLength);
        fgets(name, nameLength, file);
       
        //printf("name: %s\n\n", name);

        routers[i].id = router_id;
        routers[i].FLAGG = FLAGG;
        routers[i].nameLength = nameLength;
        routers[i].name = name;

//        free(name);
    }
    fclose(file);
    return routers;
}

void printRouters(struct Routers *routers) {
    
    int i;
    for(i = 0; i < 5; i++) {
        printf("\nid: \t\t%d\n", routers[i].id);
        printf("nameLen: \t%d\n", routers[i].nameLength);
        printf("name: \t\t%s\n", routers[i].name);
    }

}


int main(int argc, char** argv) {
    if (argc != 3) {
        printf("FAILURE: Takes 2 arguments!\n");
        exit(EXIT_FAILURE);
    }

    char *router_info = argv[1];
    char *router_cmds = argv[2];
    
    printf("\nRouter info file: \t%s\n", router_info);
    printf("Router commands file: \t%s\n\n", router_cmds);

    Router *routers = create_nodes_from_file(router_info);
    printRouters(routers);
    
    
    free(routers);
    return 0;
//    create_nodes_from_file();
//    exec_commands();
//    write_to_file();
//    exit(0);

}
